#!/usr/bin/env python2.7

import os
import sqlite3
import sys

# Student

class Student(object):
    def __init__(self, netid, classification, first_name, last_name):
        self.netid          = netid
        self.classification = classification
        self.first_name     = first_name
        self.last_name      = last_name

    def __str__(self):
        return '{:8} {:8} {:10} {}'.format(self.netid, self.classification, self.first_name, self.last_name)

# Students List Database

class StudentListDatabase(object):
    def __init__(self, path):
        self.students = []
        for line in open(path):
            netid, classification, first_name, last_name = line.strip().split('\t')
            self.students.append(Student(netid, classification, first_name, last_name))

    def search(self, field, query):
        matches = []

        for student in self.students:
            if field == 'netid' and student.netid == query:
                matches.append(student)
            elif field == 'classification' and student.classification == query:
                matches.append(student)
            elif field == 'first_name' and student.first_name == query:
                matches.append(student)
            elif field == 'last_name' and student.last_name == query:
                matches.append(student)

        return matches

# Students SQLite Database

class StudentSqliteDatabase(object):

    def __init__(self, path):
        self.conn = sqlite3.connect('students.db')

        with self.conn:
            curs      = self.conn.cursor()
            statement = '''
            CREATE TABLE IF NOT EXISTS Students(
            netid           TEXT NOT NULL PRIMARY KEY,
            classification  TEXT NOT NULL,
            first_name      TEXT NOT NULL,
            last_name       TEXT NOT NULL
            )
            '''
            curs.execute(statement)

            for line in open(path):
                netid, classification, first_name, last_name = line.strip().split('\t')
                statement = '''
                INSERT OR REPLACE INTO Students (netid, classification, first_name, last_name) VALUES (?, ?, ?, ?)
                '''
                curs.execute(statement, (netid, classification, first_name, last_name))
    
    def search(self, field, query):
        matches = []

        with self.conn:
            curs      = self.conn.cursor()
            statement = 'SELECT * FROM Students WHERE {}=?'.format(field)
            for row in curs.execute(statement, (query,)):
                matches.append(Student(row[0], row[1], row[2], row[3]))

        return matches

# Main Execution

if __name__ == '__main__':
    if len(sys.argv) != 5:
        print 'usage: {} TYPE TSV_FILE FIELD QUERY'.format(sys.argv[0])
        sys.exit(1)

    tsv_file = sys.argv[2]
    field    = sys.argv[3]
    query    = sys.argv[4]

    if sys.argv[1] == 'sql':
        students = StudentSqliteDatabase(tsv_file)
    else:
        students = StudentListDatabase(tsv_file)

    for student in students.search(field, query):
        print student
