#!/usr/bin/env python2.7

import os
import yaml

NETIDS = []
GROUPS = []

for file_name in sorted(os.listdir('.')):
    if file_name.startswith('draw'):
        netid = file_name[5:-4]

        NETIDS.append({
            'name': netid,
            'link': 'static/img/project01/{}'.format(file_name),
        })
    elif file_name.startswith('rpi'):
        group = file_name[:-4]
        GROUPS.append({
            'name': group,
            'link': 'static/img/project01/{}'.format(file_name),
        })

print yaml.dump({'netids': NETIDS, 'groups': GROUPS}, default_flow_style=False)
