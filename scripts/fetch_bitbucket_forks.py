#!/usr/bin/env python2.7

import getpass
import requests
import yaml

#username    = raw_input('Username: ')
#repository  = raw_input('Repository: ')

username     = 'CDT-30010-FA15'
password     = getpass.getpass('Password: ')
repository   = 'cdt-30010-fa15'

request_url  = 'https://bitbucket.org/api/2.0/repositories/{0}/{1}/forks'.format(username, repository)
request      = requests.get(request_url, auth=(username, password), params={'pagelen': 100})
repositories = request.json()['values']
forks        = []

for repository in sorted(repositories, key=lambda o: o['full_name']):
    owner = repository['owner']
    forks.append({
        'repository': repository['full_name'],
        'username'  : owner['username'],
        'fullname'  : owner['display_name'],
    })

forks = sorted(forks, key=lambda o: o['fullname'].split()[-1])

print yaml.safe_dump(forks, encoding='utf-8', allow_unicode=True, default_flow_style=False)
